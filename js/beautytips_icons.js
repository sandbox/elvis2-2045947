/**
 * Adds beautytip icons to a specific element on the page.
 */
(function ($) {
  Drupal.behaviors.beautytips_icons = {
    attach: function(context, settings) {

      var beautytips = Drupal.settings.beautytips;

      // Add the icon to the page
      for (var key in beautytips) {

        var tip = beautytips[key];
        var icon = tip['icon'];

        // Only do something if icon exist.
        if(icon) {

          // Decide which is the main selector.
          var selector = tip['cssSelect_original'];
          var new_selector =  tip['cssSelect'];
          var selector_clean = tip['cssSelect_clean'];

          var parent = $(selector).parent(); // We might need this later. elvis2
          var last = $(selector + ":last");
          var lastTag = $(last).attr('tagName');
          var bti_class = "bti-" + icon['icon_size'] + " " + selector_clean;

          // More vars based on icon.
          var placement = icon['placement'];
          var position = icon['position'];
          var subElement = icon['sub-element'];
          var cssClass = icon['class'] ? icon['class'] : 'custom';

          // Save for reference.
          // $(tip['cssSelect']).after(code);
          // $(tip['cssSelect']).before(code);
          // $(tip['cssSelect']).append(code);

          // Have we altered the cssSelect value? We would only do this when
          // dealing with a special element type like a select or maybe an input.

          if(lastTag == 'SELECT') {
            // Now append the icon to the cssSelect.
            var code = "<div class='select " + bti_class + " " + cssClass + "'></div>";
            $(selector).after(code);
            $(new_selector).css("background-image", "url('/"+icon['icon']+"')");
          }

          // Deal with special cases.
          if(placement && subElement) {

            var code = "<div class='" + bti_class + " " + cssClass + "'></div>";
            selector = selector + ' ' + subElement;

            // Move the beautytip icon inside of selector.
            if(placement == 'inside') {

              // Before or After the new sub-element?
              if(position == 'before') {
                $(selector).before(code);
                $(new_selector).css("background-image", "url('/"+icon['icon']+"')");
              }

              if(position == 'after') {
                $(selector).after(code);
                $(new_selector).css("background-image", "url('/"+icon['icon']+"')");
              }
            }

            if(placement == 'outside') {

              // Before or After the new sub-element?
              if(position == 'before') {
                $(selector).before(code);
                $(new_selector).css("background-image", "url('/"+icon['icon']+"')");
              }

              if(position == 'after') {
                $(selector).after(code);
                $(new_selector).css("background-image", "url('/"+icon['icon']+"')");
              }
            }

          }
          else {
            $(selector).addClass(bti_class);
            $(selector).css("background-image", "url('/"+icon['icon']+"')");
          }

        }
      }
    }
  }

})(jQuery);
